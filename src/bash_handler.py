#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os.path import exists
from os import environ


class BashHandler:
    home_path = environ['HOME']
    TERMINALS = {
        'bash': {
            'conf_file_name': '.bashrc',
            'conf_path': home_path,
            'path': '/usr/bin/'
        },
        'zsh': {
            'conf_file_name': '.zshrc',
            'conf_path': home_path,
            'path': '/usr/bin/'
        }
    }
    current_bash = TERMINALS['bash'] # terminal por defecto
    bash_file = None

    def __init__(self, mode='r'):
        for (terminal, info) in self.TERMINALS.items():
            if terminal in environ['SHELL']:
                if exists(info['conf_path'] + '/' + info['conf_file_name']):
                    self.current_bash = info
                    self.bash_file = self.__open_bash_conf_file()
                    break

    def __enter__(self):
        return self.bash_file

    def __exit__(self):
        self.bash_file.close()

    def __open_bash_conf_file(self, mode='r'):
        p = self.current_bash['conf_path'] + '/' + self.current_bash['conf_file_name']
        try:
            return open(p, mode)
        except PermissionError:
            print('¡Error! permisos del archivo de configuración:' + p)
        except FileNotFoundError:
            print('¡Error! el archivo: "' + p + '" no existe')

    def add_lines(self, lines):
        pass

    def add_alias(self, alias):
        pass

    def add_to_path(self, new_path):
        pass

    def replace_word(self, word):
        pass
